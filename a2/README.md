# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Assignment #2 Requirements:

1. Completion on Skill Sets 1-3
2. Multiple Choice Answers
3. Tip Calculator app

#### README.md file should include the following items:

* Screenshot of Tip Calculator with no entries
* Screenshot of Tip Calculator with entries and results

#### Assignment Screenshots:

*Screenshot of Tip Calculator with no entries:

![a2appideal](https://bitbucket.org/cjg18/lis4331/raw/002f762fccd2af53fda8e8b53ee968dde0c0de6e/a2/pictures/a2appideal.PNG)

*Screenshot of Tip Calculator with entries and results*:

![a2apprunning](https://bitbucket.org/cjg18/lis4331/raw/002f762fccd2af53fda8e8b53ee968dde0c0de6e/a2/pictures/a2apprunning.PNG)

*Screenshot of ss1:

![ss1](https://bitbucket.org/cjg18/lis4331/raw/b07d245588613dd03331ac517304f32c9d8fc8b5/a2/pictures/ss1.png)

*Screenshot of ss2:

![ss2](https://bitbucket.org/cjg18/lis4331/raw/b07d245588613dd03331ac517304f32c9d8fc8b5/a2/pictures/ss2.png)

*Screenshot of ss3:

![ss3](https://bitbucket.org/cjg18/lis4331/raw/b07d245588613dd03331ac517304f32c9d8fc8b5/a2/pictures/ss3.png)