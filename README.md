> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile App Development

## Christian Giordano

### LIS4331 Requirements:

*Course Work Links:*

[A1 README.md](a1/README.md "My A1 README.md file")

- Install JDK
- Install Android Studio and create My First App and Contacts App
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutorials (bitbucketstationlocation)
- Provide git command descriptions

[A2 README.md](a2/README.md "My A2 README.md file")

- Completion on Skill Sets 1-3
- Multiple Choice Answers
- Tip Calculator app
- Provide screenshots of app

[A3 README.md](a3/README.md "My A3 README.md file")

- Completion on Skill Sets 4-6
- Multiple Choice Answers
- Currency Converter app
- Provide screenshots of app

[P1 README.md](p1/README.md "My AP1README.md file")

- Completion on Skill Sets 7-9
- Multiple Choice Answers
- My Music app
- Provide screenshots of app

[A5 README.md](a5/README.md "My A5 README.md file")

- Completion on Skill Sets 13-15
- Multiple Choice Answers
- News Activity app
- Provide screenshots of app

[P2 README.md](p2/README.md "My P2 README.md file")

- Multiple Choice Answers
- My Users app
- Provide screenshots of app