# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Assignment #5 Requirements:

1. Completion on Skill Sets 13-15
2. Multiple Choice Answers
3. News Activity app
4. Provide screenshots of app

#### README.md file should include the following items:

* Screenshot of News Activity home screen
* Screenshot of News Activity more information and read more page
* Screenshot of Article the app linked to
* Screenshots of Skill Sets 13-15

#### Assignment Screenshots:

Screenshot of News Activity home:

![appa5a](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appa5a.png)

Screenshot of News Activity more information and read more page

![appa5b](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appa5b.png)

Screenshot of Article the app is linked to

![appa5c](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appa5c.PNG)



Screenshots of ss13:

![appss13](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appss13.png)

Screenshots of ss14:

![appss14](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appss14.png)

Screenshots of ss15:

![appss15](https://bitbucket.org/cjg18/lis4331/raw/81af91d8bf060c5ca09a3ff2e83a1f394366e179/a5/photos/appss15.png)


