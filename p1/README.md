# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Project 1 Requirements:

1. Completion on Skill Sets 7-9
2. Multiple Choice Answers
3. My Music app
4. Provide screenshots of app

#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of follow up screen
* Screenshot of play and pause screen
* Screenshots of Skill Sets 7-9

#### Assignment Screenshots:

Screenshot of splash screen:

![p1splash](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/p1splash.png)

Screenshot of opening screen:

![openingscreen](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/openingscreen.png)

Screenshot of playing screen

![playingscreen](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/playingscreen.png)

Screenshot of pause screen:

![pausescreen](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/pausescreen.png)



Screenshots of Skill Set 7:

![ss7a](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss7a.png)

![ss7b](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss7b.png)



Screenshots of Skill set 8:

![ss8a](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss8a.png)

![ss8b](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss8b.png)



Screenshots of Skill Set 9:

![ss9a](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss9a.png)

![ss9b](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss9b.png)

![ss9c](https://bitbucket.org/cjg18/lis4331/raw/c9e920a439bcbaeb9aa2c00b11d921d4b2337074/p1/photos/ss9c.png)