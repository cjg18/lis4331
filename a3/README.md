# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Assignment #3 Requirements:

1. Completion on Skill Sets 4-6
2. Multiple Choice Answers
3. Currency Converter app
4. Provide screenshots of app

#### README.md file should include the following items:

* Screenshot of Currency Converter splash screen
* Screenshot of Currency Converter unpopulated UI
* Screenshot of Currency Converter Toast notification
* Screenshot of Currency Converter converted currency
* Screenshots of Skill Sets 4-6

#### Assignment Screenshots:

Screenshot of Currency Converter splash screen:

![a3splash](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/a3splash.png)

Screenshot of Currency Converter unpopulated UI:

![a3unpopulated](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/a3unpopulated.png)

Screenshot of Currency Converter Toast notification:

![a3toast](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/a3toast.png)

Screenshot of Currency Converter converted currency:

![a3currency](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/a3currency.png)

Screenshots of ss4:

![ss4](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss4.png)

Screenshots of ss5:

![ss5a](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss5a.png)

![ss5b](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss5b.png)

![ss5c](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss5c.png)

Screenshots of ss6:

![ss6a](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss6a.png)

![ss6b](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss6b.png)

![ss6c](https://bitbucket.org/cjg18/lis4331/raw/61f2a98f58078c91191122bd9e62352bc0f977aa/a3/pictures/ss6c.png)