# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Project 2 Requirements:

2. Multiple Choice Answers
3. My Users app
4. Provide screenshots of app

#### README.md file should include the following items:

* Screenshot of My Users Splash Screen
* Screenshot of My Users Add, update, view, and delete options

#### Assignment Screenshots:

Screenshot of Splash Screen:

![appp2splash](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2splash.png)

Screenshot of Add option

![appp2add](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2add.png)

Screenshot of Update option

![appp2update](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2update.png)



Screenshots of View option:

![appp2view](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2view.png)

Screenshots of Delete option:

![appp2del](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2del.png)

![appp2del2](https://bitbucket.org/cjg18/lis4331/raw/e850c51a283bd7d052108f53fdf4bd1bedbe2d09/p2/pictures/appp2del2.png)


