> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Advanced Mobile App Development

## Christian Giordano

### Assignment #1 Requirements:

*Development Installations:*

1. Java JDK
2. Android Studio
3. Bitbucket Account
4. Granted Read-Only access

#### README.md file should include the following items:

* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* Screenshot of running Android Studio Contacts App
* Git commands with descriptions
* Bitbucket repo links

> 
> 
> #### Git commands w/short descriptions:

1. git init – start a new repository.
2. git status – List all files that have been committed.
3. git add – Adds a file to the “staging area”.
4. git commit – Records the file permanently .
5. git push – sends the committed changes to the remote repository.
6. git pull – pulls data from remote repository and places it in your local directory.
7. git clone – allows you to obtain a repository from an existing URL.

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![HelloWorld](https://bitbucket.org/cjg18/lis4331/raw/fde501629b6e5102c01baab0f6390b4bdffcad83/a1/pictures/HelloWorld.PNG)

*Screenshot of Android Studio - My First App*:

![myfirstapp](https://bitbucket.org/cjg18/lis4331/raw/0342ed3edb128912a2efab53ea4fb8ecc2c9b4b4/a1/pictures/myfirstapp.png)



*Screenshot of Android Studio - Contacts App:

![Contactspic1](https://bitbucket.org/cjg18/lis4331/raw/c76a9b51896919a5a54d89ee39ed34322bdb441d/a1/pictures/Contactspic1.png)

![Contactspic2](https://bitbucket.org/cjg18/lis4331/raw/c76a9b51896919a5a54d89ee39ed34322bdb441d/a1/pictures/Contactspic2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cjg18/bitbucketstationlocations/ "Bitbucket Station Locations")


